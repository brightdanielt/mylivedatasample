package com.example.mylivedatasample

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext

/**
 * A source of data for ViewModel, showcasing different LiveData + Coroutines patterns.
 * */
class DefaultDataSource constructor(
    private val ioDispatcher: CoroutineDispatcher
) : DataSource {

    /**
     * Usage of LiveData builder.
     * */
    override fun getCurrentTime(): LiveData<Long> = liveData {
        while (true) {
            delay(1000)
            emit(System.currentTimeMillis())
        }
    }

    private val weatherConditions = listOf("Sunny", "Cloudy", "Rainy", "Stormy", "Snowy")

    /**
     * Expose a LiveData of changing weather conditions every 2 secs.
     * This liveData is to showcase emit and emitSource in ViewModel.
     * */
    override fun fetchWeather(): LiveData<String> = liveData {
        var index = 0
        while (true) {
            delay(2000)
            emit(weatherConditions[index % weatherConditions.size])
            index++
        }
    }

    /**
     * Cache + Remote data source
     * */
    private val _cachedData = MutableLiveData("This is old data.")
    override val cachedData = _cachedData

    override suspend fun fetchNewData() {
        //let's see what will happen if not sue Dispatchers.Main
        //and we get "java.lang.IllegalStateException: Cannot invoke setValue on a background thread"
        withContext(Dispatchers.Main) {
            _cachedData.value = "Loading..."
            _cachedData.value = simulateNetworkDataFetch()
        }
    }

    private var requestCount = 0

    /**
     * Use ioDispatcher to simulate a long and expensive operation.
     * */
    private suspend fun simulateNetworkDataFetch(): String = withContext(ioDispatcher) {
        delay(3000)
        requestCount++
        "New data from request #$requestCount"
    }
}

interface DataSource {
    fun getCurrentTime(): LiveData<Long>
    fun fetchWeather(): LiveData<String>
    val cachedData: LiveData<String>
    suspend fun fetchNewData()
}