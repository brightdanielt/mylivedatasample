package com.example.mylivedatasample

import androidx.lifecycle.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.*

/**
 * Showcases different patterns using the LiveData coroutines builder.
 * */
class LiveDataViewModel constructor(
    private val dataSource: DataSource
) : ViewModel() {

    companion object {
        const val LOADING = "Loading..."
    }

    //Expose LiveData from a function that returns a liveData generated with a LiveData builder.
    val currentTime = dataSource.getCurrentTime()

    /**
     * To transform a liveData to another liveData, use switchMap.
     * */
    var currentTimeTransformed: LiveData<String> = currentTime.switchMap {
        //timestampToDate is a suspend function so we need to call it from a coroutine.
        liveData { emit(timeStampToDate(it)) }
    }

    /**
     * Exposed a liveData that emits a single value and subsequent values from another source.
     */
    var currentWeather: LiveData<String> = liveData {
        emit(LOADING)
        emitSource(dataSource.fetchWeather())
    }

    /**
     * Exposed cached value in the data source that can be updated later on.
     * */
    var cachedValue = dataSource.cachedData

    /**
     * Called when the user clicks the "FETCH NEW DATA" button and it updates value in data source.
     * */
    fun refresh() {
        //Launch a coroutine that reads from a remote data source and updates cache.
        viewModelScope.launch {
            dataSource.fetchNewData()
        }
    }

    private suspend fun timeStampToDate(timestamp: Long): String {
        delay(500) //Simulate long operation
        val date = Date(timestamp)
        return date.toString()
    }
}

class ViewModelFactory : ViewModelProvider.NewInstanceFactory() {

    private val dataSource = DefaultDataSource(Dispatchers.IO)

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return LiveDataViewModel(dataSource) as T
    }
}