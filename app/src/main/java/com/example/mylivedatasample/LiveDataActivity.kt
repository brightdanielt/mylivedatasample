package com.example.mylivedatasample

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.mylivedatasample.databinding.ActivityLiveDataBinding

class LiveDataActivity : AppCompatActivity() {

    //obtain ViewModel
    private val viewModel: LiveDataViewModel by viewModels { ViewModelFactory() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //Obtain binding object using the dataBinding library.
        val binding = DataBindingUtil.setContentView<ActivityLiveDataBinding>(
            this, R.layout.activity_live_data
        )

        //Set LifecycleOwner to be able to observe livedata.
        binding.lifecycleOwner = this

        //Bind ViewModel
        binding.viewmodel = viewModel

    }
}