What's this?
=============
Ａn imitation of [LiveDataSample] in [architecture-components-samples].

Why I do this?
=============
To improve my programing skill. Imitation is a simple way to learn.

Why architecture-components-samples.
=============

[architecture-components-samples] and [Codelabs] are my main learning portals, compare to other
tutorials on internet,
<br> the [Codelabs] is really great, here are my opinions：

* There are application scenarios make the tutorials necessary.
* Rich pictures make articles better understand.
* They are step by step and learning progress are recorded make learning less troublesome.
* There are document references help learner get further and correct knowledge.
* The designer are great Android developer you can also find their articles and follow them in
  Medium.
  <br>Here are my following [androiddevelopers], [Jose Alcérreca], [Florina Muntenescu],
  [Lyla Fujiwara], [Sean McQuillan].
* The designers build tutorials as a tutorial learner. ( They know what you think 😏 )

Build.gradle
=============
Although this is about LiveData, you can find there is a versions.gradle file,
<br>that's what I never do before and it's a good idea so I check [Gradle build script] and take the
course [Gradle for Android and Java] <br> suggested by [Android guides].

Beside this, I want to say...
=============
After the tutorial if I want more, I will search related topic in [Medium]
and [architecture-components-samples],
<br>then I read it and imitate it however I didn't record what I do and why I do this so I try to do
it this time
<br>and it's a kind of communication with myself, besides, I am not unfamiliar with LiveData and it
make this record high efficiency.

I love beautiful things and I know programming makes my thinking beautiful 🍀🍀🍀

[architecture-components-samples]:https://github.com/android/architecture-components-samples

[LiveDataSample]:https://github.com/android/architecture-components-samples/tree/main/LiveDataSample

[Codelabs]:https://codelabs.developers.google.com/?hl=en&cat=android

[Medium]:https://medium.com

[androiddevelopers]:https://medium.com/androiddevelopers

[Jose Alcérreca]:https://medium.com/@JoseAlcerreca

[Florina Muntenescu]:https://medium.com/@florina.muntenescu

[Lyla Fujiwara]:https://medium.com/@lylalyla

[Sean McQuillan]:https://medium.com/@objcode

[Gradle build script]:https://docs.gradle.org/current/userguide/writing_build_scripts.html#sec:extra_properties

[Gradle for Android and Java]:https://www.udacity.com/course/ud867

[Android guides]:https://developer.android.com/guide
